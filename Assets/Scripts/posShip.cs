﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posShip : MonoBehaviour {
    // stockage des angles min et max de la caméra
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private Vector3 size;

    void Start () {
        // calcul des angles avec conversion du monde de la caméra au monde du pixel pour chaque coin
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }
	

	void Update () {
        // calcule de la taille du sprite auquel ce script est attaché
        // taille normal = gameObject.GetComponent<SpriteRenderer>().bounds.size
        // On prend en compte les eventuelles transformations demandes lors de l'intégration dans le sprite dans l'éditeur de
        // siz vaut alors la taille normale * par les dféformations (notamment le zoom)
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        // Positionnement du vaisseau contre le bord gauche de l'écran (le bloque contre la gauche)
        gameObject.transform.position = new Vector3 (leftTopCameraBorder.x + (size.x / 2),
                                                     transform.position.y,
                                                     transform.position.z);
        // Fait référence à la transformation (le déplacement)
        // siz.y si t'as la moitié du sprite qui dépasse ?
        // Si la position en Y de notre vaisseau est inférieur à la limite basse de l'écran, on positionne le vaisseau en bas de l'écran
        if (transform.position.y < leftBottomCameraBorder.y + (size.y / 2))
        {
            gameObject.transform.position = new Vector3(transform.position.x,
                                                        leftBottomCameraBorder.y + (size.y / 2),
                                                        transform.position.z);
        }
        // Si la position en Y de notre vaisseau est supérieur à la limite haute de l'écran, on repositionne le vaisseau en haut de l'écran
        if (transform.position.y > leftTopCameraBorder.y - (size.y / 2))
        {
            gameObject.transform.position = new Vector3(transform.position.x,
                                                        leftTopCameraBorder.y - (size.y / 2),
                                                        transform.position.z);

        }
    }
}
