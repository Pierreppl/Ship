﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveShoot : MonoBehaviour
{
    public Vector2 vitesse;
    public Vector2 mouvement;
    private Vector3 size;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private float angle;

    void Start()
    {
        // calcul des angles avec conversion du monde de la caméra au monde du pixel pour chaque coin
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        angle = 1;
    }

    void Update()
    {
        // Déplacement de droite à gauche à une vitesse constante de 2
        GetComponent<Rigidbody2D>().velocity = mouvement;
        // Calcul de la taille du sprite auquel ce scripte est attaché
        // Taille normal = gameObject.GetComponent<SpriteRenderer> ().bounds.size;
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        // Si le sprite sort de l'écran à gauche
        // Recalcul d'une nouvelle position en Y comprise dans les limites autorisés de l'écran
        // destroy de l'objet
        if ((transform.position.x > rightBottomCameraBorder.x - (size.x / 2) && gameObject.tag != "bonus") || (transform.position.x < leftBottomCameraBorder.x - (size.x / 2)))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //collision.gameObject.AddComponent<fadeOut>();
        if (gameObject.tag == "shootJaune")
        {
            if (collision.tag == "Player")
            {
                Destroy(gameObject);
                if (GameObject.FindGameObjectWithTag("life5"))
                {
                    GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
                }
                else if (GameObject.FindGameObjectWithTag("life4"))
                {
                    GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
                }
                else if (GameObject.FindGameObjectWithTag("life3"))
                {
                    GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
                }
                else if (GameObject.FindGameObjectWithTag("life2"))
                {
                    GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
                }
                else if (GameObject.FindGameObjectWithTag("life1"))
                {
                    GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
                    SceneManager.LoadScene("Scene-Score");
                }
            }
        }
        else if(gameObject.tag == "bonus")
        {
            if (collision.tag == "Player")
            {
                GameState.Instance.addTirBonus(10);
                Destroy(gameObject);
            }
        }
        else
        {
            if (collision.tag != "Player" && gameObject.tag != "shootBleu")
            {
                Destroy(gameObject);
            }
        }

    }
}