﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gestionAsteroid : MonoBehaviour {
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private GameObject[] respawns;
    private Vector2 mouvement;
    private float rotation;
    private Vector3 size;
    public int vie;

	void Start () {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        vie = Random.Range(1, 4);
        initialisationMouvementRotation();
    }


    void Update () {

        gestionMouvement();
        gestionSpawn();
    }

    private void gestionSpawn()
    {
        respawns = GameObject.FindGameObjectsWithTag("asteroid");
        if (respawns.Length > 0)
        {
            size.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x * gameObject.transform.localScale.x;
            size.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y * gameObject.transform.localScale.y;
        }
        if (respawns.Length < 10)
        {
            if (Random.Range(1, 100) == 50 || respawns.Length < 4)
            {
                Vector3 posTemporaire = new Vector3(rightBottomCameraBorder.x + (size.x / 2) + Random.Range(0, 10),
                                                       Random.Range(rightBottomCameraBorder.y + (size.y / 2), (rightTopCameraBorder.y - (size.y / 2))),
                                                       transform.position.z);
                GameObject gY = Instantiate(Resources.Load("asteroidSP"), posTemporaire, Quaternion.identity) as GameObject;
            }
        }
    }

    private void gestionMouvement()
    {
        GetComponent<Rigidbody2D>().velocity = mouvement;
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        GetComponent<Rigidbody2D>().transform.Rotate(0, 0, rotation);
        if (transform.position.x < leftBottomCameraBorder.x - (size.x / 2))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "shootOrange")
        {
            vie--;
            if(vie == 0)
            {
                gameObject.AddComponent<explosionAsteroid>();
                GameState.Instance.addScorePlayer(1);
            }
        }
        else if(collision.tag == "shootBleu")
        {
            vie = 5;
            gameObject.AddComponent<explosionAsteroid>();
            GameState.Instance.addScorePlayer(1);
        }
        if (collision.name == "myShip")
        {
            gameObject.AddComponent<explosionAsteroid>();
            if (GameObject.FindGameObjectWithTag("life5"))
            {
                GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life4"))
            {
                GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life3"))
            {
                GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life2"))
            {
                GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life1"))
            {
                GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
                SceneManager.LoadScene("Scene-Score");
            }
        }
    }

    private void initialisationMouvementRotation()
    {
        rotation = generationNombre();
        mouvement = new Vector2(-Random.Range(2, 5), 0);
    }

    private float generationNombre()
    {
        float nombre = 0;
        int choixOperateur = Random.Range(0, 2);
        if (choixOperateur == 0)
        {
            nombre = -Random.Range(2, 5);
        }
        else if (choixOperateur == 1)
        {
            nombre = Random.Range(2, 5); ;
        }
        return nombre;
    }
}
