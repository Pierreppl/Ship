﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gestionVaisseau : MonoBehaviour {
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    private Vector3 size;
    public Vector2 mouvement;

    void Start () {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        size.x = GetComponent<SpriteRenderer>().bounds.size.x * gameObject.transform.localScale.x;
        size.y = GetComponent<SpriteRenderer>().bounds.size.y * gameObject.transform.localScale.y;
        InvokeRepeating("gestionTir", 0, Random.Range(0.3f,5f));
    }

    void Update()
    {
        gestionMouvement();
    }

    private void gestionTir()
    {
        Vector3 posTemporaire = new Vector3(transform.position.x - size.x / 2.3f,
                         transform.position.y,
                         transform.position.z);
        GameObject gY = Instantiate(Resources.Load("shootJaune"), posTemporaire, Quaternion.identity) as GameObject;
    }

    private void gestionMouvement()
    {
        GetComponent<Rigidbody2D>().velocity = mouvement;
        if (transform.position.x < leftBottomCameraBorder.x - (size.x / 2))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            gameObject.AddComponent<explosionAsteroid>();
            if (GameObject.FindGameObjectWithTag("life5"))
            {
                GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life4"))
            {
                GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life3"))
            {
                GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life2"))
            {
                GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life1"))
            {
                GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
                SceneManager.LoadScene("Scene-Score");
            }
        }
        else if(collision.tag == "shootOrange" || collision.tag == "shootBleu")
        {
            gameObject.AddComponent<explosionAsteroid>();
            GameState.Instance.addScorePlayer(10);
        }
    }
}
