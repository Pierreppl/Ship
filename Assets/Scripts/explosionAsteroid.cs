﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosionAsteroid : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        GameObject expl = Instantiate(Resources.Load("asteroidExplosion"), transform.position, Quaternion.identity) as GameObject;
        Destroy(gameObject);
        expl.AddComponent<fadeOut>();
    }
}
