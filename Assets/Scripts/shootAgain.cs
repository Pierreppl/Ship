﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class shootAgain : MonoBehaviour {
    private Vector3 spriteSize;

    void Update ()
    {
        // Récupération de la taille de l'objet auquel est acché ce script (le vaisseau)
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        // Si il appuie sur la touche espace
        if (!GameState.Instance.isPaused())
        {
            if (((Input.touchCount > 0 &&
                Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0)).x > Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).x / 3
                ) || Input.GetKeyDown(KeyCode.Space)))
            {
                Vector3 posTemporaire = new Vector3(transform.position.x + spriteSize.x/2.5f,
                                    transform.position.y,
                                    transform.position.z);

                // Appel du son de tir
                AudioSource audio = GetComponent<AudioSource>();
                audio.Play();
                // Création d'une instance d'un préfav de type shootOrange ou shootBleu en fonction du nombre de tirs bonus
                if(GameState.Instance.getTirBonus() > 0)
                {
                    GameObject gY = Instantiate(Resources.Load("shootBleu"), posTemporaire, Quaternion.identity) as GameObject;
                    GameState.Instance.addTirBonus(-1);
                }
                else
                {
                    GameObject gY = Instantiate(Resources.Load("shootOrange"), posTemporaire, Quaternion.identity) as GameObject;
                }
            }
        }
    }
}
