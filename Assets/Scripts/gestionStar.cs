﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gestionStar : MonoBehaviour {
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    public Vector3 size;
    public Vector2 mouvement;

    void Start ()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }
	
	void Update ()
    {
        gestionMouvement();
    }

    void gestionMouvement()
    {
        GetComponent<Rigidbody2D>().velocity = mouvement;
        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        if (transform.position.x < leftBottomCameraBorder.x - (size.x / 2))
        {
            gameObject.transform.position = new Vector3(rightBottomCameraBorder.x + (size.x / 2),
                                            transform.position.y,
                                            transform.position.z);
        }
    }
}
