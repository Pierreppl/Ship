﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class GameState : MonoBehaviour {
    public int scorePlayer;
    private string namePlayer = "";
    private bool paused = false;
    private int tirBonus = 0;
    private GameObject[] vaisseaux;
    private GameObject[] bonus;
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    public static GameState instance = null;

    public static GameState Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        namePlayer = PlayerPrefs.GetString("namePlayer");
    }

    void Start ()
    {
        scorePlayer = 0;
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
	}

    // Mise à jour du score dans la scène 3
    void FixedUpdate()
    {
        if (GameObject.FindGameObjectWithTag("playerLabel") != null)
        {
            GameObject.FindGameObjectWithTag("playerLabel").GetComponent<Text>().text = namePlayer;
        }
        if (GameObject.FindGameObjectWithTag("scoreLabel") != null)
        {
            GameObject.FindGameObjectWithTag("scoreLabel").GetComponent<Text>().text = "" + scorePlayer;
        }
        if (GameObject.FindGameObjectWithTag("tirLabel") != null)
        {
            GameObject.FindGameObjectWithTag("tirLabel").GetComponent<Text>().text = "Tirs bonus : " + tirBonus;
        }
         if (SceneManager.GetActiveScene().name != "Scene-Score" && SceneManager.GetActiveScene().name != "Scene-Menu")
        {
            spawnVaisseau();
            spawnBonus();
        }

    }

    void Update()
    {
        AudioSource audio = GetComponent<AudioSource>();
        //Mode pause si l'utilisateur appuie sur P ou touche avec 3 doigts l'écran
        if (Input.GetKeyDown("p") || Input.touchCount >= 3)
        {
            if (!paused)
            {
                Time.timeScale = 0;
                audio.Pause();
            }
            else
            {
                Time.timeScale = 1;
                audio.Play();
            }
            paused = !paused;
        }
    }

    private void spawnBonus()
    {
        vaisseaux = GameObject.FindGameObjectsWithTag("bonus");
        if (Random.Range(0, 600) == 50 && vaisseaux.Length <= 1)
        {
            Vector3 posTemporaire = new Vector3(rightBottomCameraBorder.x + Random.Range(0, 10),
                                       Random.Range(rightBottomCameraBorder.y, (rightTopCameraBorder.y)));
            GameObject gY = Instantiate(Resources.Load("powerup"), posTemporaire, Quaternion.identity) as GameObject;
        }
    }

    private void spawnVaisseau()
    {
        vaisseaux = GameObject.FindGameObjectsWithTag("vaisseau");
        if (Random.Range(0, 200) == 50 && vaisseaux.Length <= 3)
        {
            Vector3 posTemporaire = new Vector3(rightBottomCameraBorder.x + Random.Range(0, 10),
                                       Random.Range(rightBottomCameraBorder.y, (rightTopCameraBorder.y)));
            GameObject gY = Instantiate(Resources.Load("vaisseau"), posTemporaire, Quaternion.identity) as GameObject;
        }
    }

    public void addScorePlayer(int nombre)
    {
        scorePlayer += nombre;
    }

    public int getScorePlayer()
    {
        return scorePlayer;
    }

    public void setNamePlayer(string name)
    {
        namePlayer = name;
    }

    public string getNamePlayer()
    {
        return namePlayer;
    }

    public void addTirBonus(int nombre)
    {
        tirBonus += nombre;
        print(tirBonus);
    }

    public int getTirBonus()
    {
        return tirBonus;
    }

    public bool isPaused()
    {
        return paused;
    }

}
