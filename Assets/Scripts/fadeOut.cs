﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeOut : MonoBehaviour {
	
	void Update () {
        Color couleur = GetComponent<SpriteRenderer>().color;
        couleur.a -= 0.01f;
        GetComponent<SpriteRenderer>().color = couleur;
        if(couleur.a < 0)
        {
            Destroy(gameObject);
        }
    }
}
