﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gestionBonus : MonoBehaviour {
    private Vector3 leftTopCameraBorder;
    private Vector3 rightTopCameraBorder;
    private Vector3 rightBottomCameraBorder;
    private Vector3 leftBottomCameraBorder;
    public GameObject[] respawns;
    private Vector3 size;

    void Start ()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    void Update()
    {
        gestionMouvement();
        respawns = GameObject.FindGameObjectsWithTag("bonus");
        if (respawns.Length > 0)
        {
            size.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x * gameObject.transform.localScale.x;
            size.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y * gameObject.transform.localScale.y;
        }
        if (respawns.Length < 10)
        {
            if (Random.Range(1, 100) == 50 && respawns.Length < 1)
            {
                Vector3 posTemporaire = new Vector3(rightBottomCameraBorder.x + (size.x / 2),
                                                       Random.Range(rightBottomCameraBorder.y + (size.y / 2), (rightTopCameraBorder.y - (size.y / 2))),
                                                       transform.position.z);
                GameObject gY = Instantiate(Resources.Load("powerup"), posTemporaire, Quaternion.identity) as GameObject;
            }
        }
    }

    void gestionMouvement()
    {

    }
}
